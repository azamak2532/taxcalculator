package com.crudapi.taxcalculator.controller

import com.crudapi.taxcalculator.domain.EmployeeTax
import com.crudapi.taxcalculator.service.EmployeeTaxService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject

@RestController
@EnableFeignClients("com.inteliquent.crudapi")
class EmployeeTaxController {

    @Autowired
    lateinit var employeeTaxService: EmployeeTaxService

    @GetMapping("/calculateTax/{id}")
    fun getTaxOfEmployee(@PathVariable ("id") id : Int): EmployeeTax? {
        val s:String = "http://localhost:8080/getEmployeeById/$id"
      return  employeeTaxService.calculateTax(RestTemplate().getForObject(s, EmployeeTax::class.java)!!)
    }

}