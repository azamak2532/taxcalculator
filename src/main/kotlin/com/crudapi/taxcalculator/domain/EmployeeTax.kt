package com.crudapi.taxcalculator.domain

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

data class EmployeeTax(
        @JsonProperty
        val empId: Int = 0,
        @JsonProperty
        val empName: String = "",
        @JsonProperty
        val empSalary: Double = 0.0,
        @JsonProperty
        val empDept: String = "",
        @JsonProperty
        var empTax: Double = 0.0
)