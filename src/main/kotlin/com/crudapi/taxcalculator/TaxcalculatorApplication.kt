package com.crudapi.taxcalculator

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication

class TaxcalculatorApplication

fun main(args: Array<String>) {
	runApplication<TaxcalculatorApplication>(*args)
}
