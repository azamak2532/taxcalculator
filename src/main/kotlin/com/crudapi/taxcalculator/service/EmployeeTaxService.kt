package com.crudapi.taxcalculator.service

import com.crudapi.taxcalculator.domain.EmployeeTax


interface EmployeeTaxService {

    fun calculateTax(e: EmployeeTax): EmployeeTax
}