package com.crudapi.taxcalculator.service

import com.crudapi.taxcalculator.domain.EmployeeTax
import org.springframework.stereotype.Service

@Service
class EmployeeTaxServiceImpl : EmployeeTaxService {
    override fun calculateTax(e: EmployeeTax): EmployeeTax {
        val i:Double = e.empSalary
        if (i > 5000) {
            e.empTax = 28.00
        } else {
            e.empTax = 24.00
        }
        return e
    }
}



